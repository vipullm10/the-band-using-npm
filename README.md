A responsive css design for a fictional band website . 
Proper development process followed using npm .
A list of all npm modules used :-

a)Run time dependecies
    "font-awesome": "^4.7.0",
    "lodash": "^4.17.15",
    "normalize.css": "^8.0.1"

b)Development dependencies 
    "autoprefixer": "^9.7.6",
    "css-loader": "^3.5.3",
    "postcss-import": "^12.0.1",
    "postcss-loader": "^3.0.0",
    "postcss-mixins": "^6.2.3",
    "postcss-nested": "^4.2.1",
    "postcss-simple-vars": "^5.0.2",
    "style-loader": "^1.2.0",
    "webpack": "^4.43.0",
    "webpack-cli": "^3.3.11",
    "webpack-dev-server": "^3.10.3"
  